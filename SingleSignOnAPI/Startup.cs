﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(SingleSignOnAPI.Startup))]
namespace SingleSignOnAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
