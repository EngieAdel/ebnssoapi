﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SingleSignOnAPI.Models;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web;
using System.Configuration;

namespace SingleSignOnAPI.Controllers
{
    public class OauthTokenController : ApiController
    {
        [HttpPost]
        public object Post(string client_id, string client_secret, string redirect_uri, string code, string grant_type )
        {
      
            try
            {

                return GetAccessToken(client_id, client_secret, redirect_uri, code, grant_type);
            }
            catch (Exception ex)
            {
                return ex.Message;
                    
            }

        }

        private static object GetAccessToken(string client_id, string client_secret, string redirect_uri, string code, string grant_type)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["tokenserver"]);

                // We want the response to be JSON.
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Build up the data to POST.
                List<KeyValuePair<string, string>> postData = new List<KeyValuePair<string, string>>();
                postData.Add(new KeyValuePair<string, string>("grant_type", grant_type));
                postData.Add(new KeyValuePair<string, string>("code", code));
                postData.Add(new KeyValuePair<string, string>("client_id", client_id));
                postData.Add(new KeyValuePair<string, string>("client_secret", client_secret));
                postData.Add(new KeyValuePair<string, string>("redirect_uri", redirect_uri));

                

                FormUrlEncodedContent content = new FormUrlEncodedContent(postData);
                var response = client.PostAsync(ConfigurationManager.AppSettings["tokenserver"], content);
                var contentString = JsonConvert.DeserializeObject(response.Result.Content.ReadAsStringAsync().Result);

                // return the Access Token.
                return contentString;
            }
        }



        private Dictionary<string, string> CreateTokenRequestElements(string clientId, string clientSecret,
 string grantType, string code, string redirectUri)
        {
            var segments = new Dictionary<string, string>();
            segments.Add("client_id", clientId);
            segments.Add("client_secret", clientSecret);
            segments.Add("grant_type", grantType);
            segments.Add("code", code);
            segments.Add("redirect_uri", redirectUri);

            return segments;
        }

    }






}