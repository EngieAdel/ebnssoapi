﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SingleSignOnAPI.Models;

namespace SingleSignOnAPI.Controllers
{
    public class authController : ApiController
    {
        [HttpGet]
        public object Get(string clientid)
        {
            //var x = Guid.NewGuid().ToString("n") + Guid.NewGuid().ToString("n");
            //  context.SetToken(x);
           var encClient = EncryptDecrypt.Encrypt(clientid );
            return encClient; 
        }
    }
}