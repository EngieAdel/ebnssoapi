﻿using System;

using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System.Collections;
using System.Linq;
using SingleSignOnAPI.Models;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Infrastructure;
using System.Threading.Tasks;
using System.Security.Claims;

using Microsoft.Owin.Security;

using System.Collections.Generic;
using System.Collections.Concurrent;

using System.Web;

namespace SingleSignOnAPI
{
    public partial class Startup
    {
        
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }
        private readonly ConcurrentDictionary<string, string> _authenticationCodes = new ConcurrentDictionary<string, string>(StringComparer.Ordinal);
     
        public static string PublicClientId { get; private set; }




        public void ConfigureAuth(IAppBuilder app)
        {
            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AuthorizeEndpointPath = new PathString("/auth"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(60),
                Provider = new AppOAuthProvider(),
                //  RefreshTokenProvider = new SimpleRefreshTokenProvider()
                AuthorizationCodeProvider = new AuthenticationTokenProvider()
                {
                    OnCreate = CreateAuthenticationCode,
                    OnReceive = ReceiveAuthenticationCode
                }
            };
            app.Use(async (context, next) =>
            {
                if (context.Request.QueryString.HasValue)
                {
                    if (string.IsNullOrWhiteSpace(context.Request.Headers.Get("Authorization")))
                    {
                        var queryString = HttpUtility.ParseQueryString(context.Request.QueryString.Value);
                        string token = queryString.Get("access_token");

                        if (!string.IsNullOrWhiteSpace(token))
                        {
                            context.Request.Headers.Add("Authorization", new[] { string.Format("Bearer {0}", token) });
                        }
                    }
                }

                await next.Invoke();
            });

            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private void CreateAuthenticationCode(AuthenticationTokenCreateContext context)
        {
            // context.SetToken(Guid.NewGuid().ToString("n") + Guid.NewGuid().ToString("n"));
            context.SetToken("123");
            _authenticationCodes[context.Token] = context.SerializeTicket();
            //Log.Info("Create a token with value: 123");
        }

        private void ReceiveAuthenticationCode(AuthenticationTokenReceiveContext context)
        {
            string value;
            if (_authenticationCodes.TryRemove(context.Token, out value))
            {
                context.DeserializeTicket(value);
                //   Log.Info("Call at ReceiveAuthenticationCode");
            }


            object form;
            // Definitely doesn't feel right
            context.OwinContext.Environment.TryGetValue(
                    "Microsoft.Owin.Form#collection", out form);
            var redirectUris = (form as FormCollection).GetValues("redirect_uri");
            var clientIds = (form as FormCollection).GetValues("client_id");
            if (redirectUris != null && clientIds != null)
            {
                // Queries the external server to validate the token
                string username = "eBNSSOTest";//await GetUserName(context.Token,
                                               //    redirectUris[0]);

                var claims = new List<Claim>();

                // Setting
                claims.Add(new Claim(ClaimTypes.Name, username));

                // Setting Claim Identities for OAUTH 2 protocol.
                ClaimsIdentity oAuthClaimIdentity = new ClaimsIdentity(claims, OAuthDefaults.AuthenticationType);
                ClaimsIdentity cookiesClaimIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);


                var authProps = new AuthenticationProperties();

                // Required. The request is rejected if it's not provided
                authProps.Dictionary.Add("client_id", clientIds[0]);

                // Required, must be in the future
                authProps.ExpiresUtc = DateTimeOffset.Now.AddDays(1);

                var ticket = new AuthenticationTicket(oAuthClaimIdentity, authProps);
                context.SetTicket(ticket);
            }

        }

    }




    public class QueryStringOAuthBearerProvider : OAuthBearerAuthenticationProvider
    {
        public override Task RequestToken(OAuthRequestTokenContext context)
        {
            var value = context.Request.Query.Get("access_token");
            if (!string.IsNullOrEmpty(value))
            {
                context.Token = value;
            }

            return Task.FromResult<object>(null);

        }

    }
}



